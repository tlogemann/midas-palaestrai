# MIDAS Palaestrai Adapter

## Description
This package contains everything needed to use midas with palaestrAI.

## License
This software is released under the GNU Lesser General Public License (LGPL). See the license file for more information about the details.
